---
title: Cahier de labo
subtitle: Là où on scribe tout
date: 2015-02-20
tags: ["example", "markdown"]
---

You can write regular [markdown](http://markdowntutorial.com/) here and Jekyll will automatically convert it to a nice webpage.  I strongly encourage you to [take 5 minutes to learn how to write in markdown](http://markdowntutorial.com/) - it'll teach you how to transform regular text into bold/italics/headings/tables/etc.

**Here is some bold text**

## Here is a secondary heading

Here's a useless table:

| **BAssin 1** | Bassin 2 | Bassin 3 | Previous number |
| :------ |:--- | :--- | :--- |
| Five | Six | Four | Four |
| Ten | Eleven | Nine | Four |
| Seven | Eight | Six | Four |
| Two | Three | One | Four |


|FIELD1        |Date de la mise en culture|Date de la prise de mesure|Heure de la prise de mesure|personne qui a pris les mesures|personne qui a documenté|ph |%brix|°C liquide|°C air|Observation|Action|
|--------------|--------------------------|--------------------------|---------------------------|-------------------------------|------------------------|---|-----|----------|------|-----------|------|
|cornifletteone|2/9/22                    |2/9/22                    |11:44                      |Calam                          |Calam                   |7  |20   |24        |24    |           |      |
|jessica4ever  |04/03/2022                |                          |                           |                               |                        |   |     |          |      |           |      |


How about a yummy crepe?

![Crepe](http://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg)

Here's a code chunk with syntax highlighting:

```javascript
var foo = function(x) {
  return(x + 5);
}
foo(3)
```
