# BIBLIOGRAPHIE

Cette bibliographie est collaborative. Elle s'appuie sur [un groupe zotero](https://www.zotero.org/groups/4833379/komb/library) que chacun.e peut utiliser, copier, étudier, modifier et dont les versions transformées peuvent être redistribuées. Zotero est un logiciel libre de gestion de bibliographie dont nous vous invitons à décrouvrir [les fonctionalités](https://www.zotero.org/). Si vous ne souhaitez pas passer par zotero, vous pouvez aussi télécharger la bibliographie qui suit au format [pdf](url) ou [odt](url). 


{{< bibliography >}}
